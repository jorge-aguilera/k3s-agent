# k3s-agent


## Objetivo

Prueba de concepto para desplegar mediante CD/CI un proyecto Gitlab en mi cluster K3S

## Requisitos

- Un cluster de kubernetes y acceso con privilegios al mismo
- Una cuenta de Gitlab
- Un proyecto que desplegar

## Preparación

- crea el proyecto git 
- crea un directorio .gitlab/agents/my-k3s
- crea un fichero *vacío* .gitlab/agents/my-k3s/config.yaml
- sube los cambios al repositorio
- en el menú de Gitlab Infraestructura/Kubernetes crea un agente nuevo (Install a new agent) 
y selecciona el que has creado (en este caso my-k3s)
- copia el comando `docker run ....` que aparece en el popup para instalar el agente en tu cluster

Si todo va bien en la lista de agentes que aparecen en esta configuración aparecerá el tuyo como
"conectado" a los pocos segundos

Ahora ya tienes dialogando a tu cluster con Gitlab y puedes hacer que un pipeline ejecute comandos
desde Gitlab en tu cluster

## Proyecto

Vamos a desplegar en nuestro cluster un "WhoAmI" :


```
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: whoami-deployment
  labels:
    app: whoami
spec:
  replicas: 3
  selector:
    matchLabels:
      app: whoami
  template:
    metadata:
      labels:
        app: whoami
    spec:
      containers:
        - name: whoami
          image: containous/whoami:latest
          ports:
            - containerPort: 80
          volumeMounts:
            - mountPath: /data
              name: mn-eess
      volumes:
        - name: mn-eess
          persistentVolumeClaim:
            claimName: 'mn-eess'
---
apiVersion: v1
kind: Service
metadata:
  name: whoami-service
spec:
  selector:
    app: whoami
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: whoami-ingress
spec:
  rules:
  - http:
      paths:
      - path: /testpath
        pathType: Prefix
        backend:
          service:
            name: whoami-service
            port:
              number: 80
```


Creamos el fichero `whoami.yml` y lo añadimos en el repositorio

## Desplegando

Añadimos al fichero `gitlab-ci.yml` donde estan definidos nuestros jobs para construir y desplegar la aplicación
un job nuevo `deploy` (en este caso no hay aplicación, simplemente vamos a desplegar el yml anterior):

```
 deploy:
   image:
     name: bitnami/kubectl:latest
     entrypoint: [""]
   script:
   - kubectl config use-context jorge-aguilera/k3s-agent:my-k3s
   - kubectl get pods
   - kubectl apply -f whoami.yml
   - kubectl get pods
```

"Simplemente" configuramos a kubectl para que use nuestro agente (un fichero vacío!!!! ni idea de donde está la magia de Gitlab)
y desplegamos el yml. Previamente hago un get pods para ver los pods de mi cluster local antes de aplicar el fichero y los que 
hay despúes


